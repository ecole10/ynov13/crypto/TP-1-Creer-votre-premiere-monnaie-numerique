DOCKER          = docker
DOCKER_COMPOSE  = docker-compose

EXEC_PHP        = $(DOCKER_COMPOSE) exec -T php /entrypoint
SHELL_PHP		= $(DOCKER_COMPOSE) exec php /entrypoint sh

COMPOSER        = $(EXEC_PHP) composer
SYMFONY         = $(EXEC_PHP) bin/console
QA              = $(DOCKER) run --rm -v `pwd`/app:/project -w /project jakzal/phpqa:php8.0

##
##Project
##-------

build:
	@$(DOCKER_COMPOSE) pull --parallel --quiet --ignore-pull-failures 2> /dev/null
	$(DOCKER_COMPOSE) build --pull

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

install: ## Install and start the project
install: build start vendor db-fixtures

clean: ## Stop the project and remove generated files
clean: kill
	rm -rf ./app/vendor ./app/var

reset: ## Stop and start a fresh install of the project
reset: clean install

start: ## Start the project
	$(DOCKER_COMPOSE) up -d --remove-orphans --no-recreate

stop: ## Stop the project
	$(DOCKER_COMPOSE) stop

restart: ## Restart the project
restart: stop start

#COMPOSER

composer.lock: ./app/composer.json
	$(COMPOSER) update --lock --no-scripts --no-interaction

vendor: ./app/composer.lock
	$(COMPOSER) install

##
##SYMFONY
##-------

secrets:
	$(SYMFONY) secrets:decrypt-to-local --force

db: ## Reset the database
db:
	@$(EXEC_PHP) php -r 'echo "Wait database...\n"; set_time_limit(30); require __DIR__."/tests/bootstrap.php"; $$u = parse_url($$_ENV["DATABASE_URL"]); for(;;) { if(@fsockopen($$u["host"].":".($$u["port"] ?? 3306))) { break; }}'
	$(SYMFONY) doctrine:database:drop --if-exists --force
	$(SYMFONY) doctrine:database:create
	$(SYMFONY) doctrine:migrations:migrate --no-interaction --allow-no-migration

db-fixtures: ## Load database fixtures
db-fixtures: db
	$(SYMFONY) doctrine:fixtures:load --no-interaction

migration: ## Generate a new doctrine migration
migration:
	$(SYMFONY) doctrine:migrations:diff

db-validate-schema: ## Validate the doctrine ORM mapping
db-validate-schema:
	$(SYMFONY) doctrine:schema:validate

##
##Tests
##-------

test: ## Run unit and functional tests
test: tu tf

tu: ## Run unit tests
tu: vendor
	$(EXEC_PHP) bin/phpunit --exclude-group functional

tf: ## Run functional tests
tf: vendor
	$(EXEC_PHP) bin/phpunit --group functional

##
##QA
##-------

lint: ## Lints twig, yaml and PHP files
lint: lt ly php-cs-fixer php-stan

lt: vendor
	$(SYMFONY) lint:twig templates

ly: vendor
	$(SYMFONY) lint:yaml config

php-cs-fixer: ## Php-cs-fixer (http://cs.sensiolabs.org)
	$(QA) php-cs-fixer fix --dry-run --using-cache=no --verbose --diff

apply-php-cs-fixer: ## Apply php-cs-fixer fixes
	$(QA) php-cs-fixer fix --using-cache=no --verbose --diff

php-stan: ## PhpStan (https://phpstan.org/)
	$(QA) phpstan analyse

security-check: ## Check vulnerability in PHP packages
security-check:
	-$(QA) local-php-security-checker

##
##Shell
##-----

shell-php: ## Access sh php container
shell-php:
	$(SHELL_PHP)

##
##Logs
##-----

logs-nginx: ## Access logs nginx container
logs-nginx:
	$(DOCKER_COMPOSE) logs nginx

logs-php: ## Access logs php container
logs-php:
	$(DOCKER_COMPOSE) logs php

logs-db: ## Access logs db container
logs-db:
	$(DOCKER_COMPOSE) logs db

##
##Documentation
##-----

.DEFAULT_GOAL := doc
doc: ## List commands available in Makefile
doc:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: doc
