<?php

namespace App\DataTransformer\Input\Transaction;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\Transaction\TransactionInputDto;
use App\Entity\Crypto;
use App\Entity\Transaction;
use App\Entity\User;
use App\Entity\Wallet;
use App\Entity\WalletCrypto;
use App\Repository\CryptoRepository;
use App\Repository\WalletRepository;
use Doctrine\ORM\EntityManagerInterface;

class TransactionInputDataTransformer implements DataTransformerInterface
{
    private ValidatorInterface $validator;
    private WalletRepository $walletRepository;
    private CryptoRepository $cryptoRepository;
    private EntityManagerInterface $em;

    public function __construct(
        ValidatorInterface $validator,
        WalletRepository $walletRepository,
        CryptoRepository $cryptoRepository,
        EntityManagerInterface $em
    ) {
        $this->validator = $validator;
        $this->walletRepository = $walletRepository;
        $this->cryptoRepository = $cryptoRepository;
        $this->em = $em;
    }

    /**
     * @param TransactionInputDto $data
     */
    public function transform($data, string $to, array $context = []): User
    {
        $this->validator->validate($data);

        $toWallet = $this->getToWallet($data->getTo());
        $crypto = $this->getCrypto($data->getCryptoIssuer());
        /** @var User $user */
        $user = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];
        $fromWallet = $user->getWallet();
        $amountToSend = $data->getValue();

        $walletCryptoFrom = $this->checkIfWalletFromIsSolvent($fromWallet, $crypto->getIssuer(), $amountToSend);
        $this->creditReceiver($toWallet, $crypto, $amountToSend);
        $this->debitSender($walletCryptoFrom, $amountToSend);

        $transaction = new Transaction();
        $transaction
            ->setFromWallet($fromWallet)
            ->setToWallet($toWallet)
            ->setCrypto($crypto)
            ->setValue($amountToSend)
            ->setCreatedAt(new \DateTimeImmutable());

        $fromWallet->addTransaction($transaction);

        return $user;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof User) {
            return false;
        }

        return User::class === $to
            && null !== ($context['input']['class'] ?? null)
            && TransactionInputDto::class === $context['input']['class'];
    }

    private function getToWallet(string $to): Wallet
    {
        $toWallet = $this->walletRepository->find($to);

        if ($toWallet === null) {
            throw new \Exception("Wallet not found");
        }

        return $toWallet;
    }

    private function getCrypto(string $cryptoIssuer): Crypto
    {
        $crypto = $this->cryptoRepository->findOneBy(['issuer' => $cryptoIssuer]);

        if ($crypto === null) {
            throw new \Exception("Crypto doesn't exist");
        }

        return $crypto;
    }

    private function checkIfWalletFromIsSolvent(Wallet $wallet, string $cryptoIssuer, float $value): WalletCrypto
    {
        $walletCrypto = $wallet->getWalletCryptoByIssuer($cryptoIssuer);

        if ($walletCrypto !== null) {
            if ($walletCrypto->getNumber() < $value) {
                throw new \Exception(
                    sprintf("You don't have enough %s", $cryptoIssuer)
                );
            }
        } else {
            throw new \Exception(
                sprintf("You don't have this crypto (%s)", $cryptoIssuer)
            );
        }

        return $walletCrypto;
    }

    private function creditReceiver(Wallet $toWallet, Crypto $crypto, float $amountToReceive): void
    {
        $walletCryptoTo = $toWallet->getWalletCryptoByIssuer($crypto->getIssuer());

        if ($walletCryptoTo !== null) {
            $walletCryptoTo->setNumber($walletCryptoTo->getNumber() + $amountToReceive);
        } else {
            $newWalletCrypto = new WalletCrypto();
            $newWalletCrypto
                ->setCrypto($crypto)
                ->setNumber($amountToReceive);

            $toWallet->addWalletCrypto($newWalletCrypto);
        }

        $this->em->persist($toWallet);
        $this->em->flush();
    }

    private function debitSender(WalletCrypto $walletCryptoFrom, float $amountToSend): void
    {
        $walletCryptoFrom->setNumber($walletCryptoFrom->getNumber() - $amountToSend);
    }
}
