<?php

namespace App\DataTransformer\Input\Crypto;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\Crypto\CryptoInputDto;
use App\Entity\Crypto;

final class CryptoInputDataTransformer implements DataTransformerInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param CryptoInputDto $data
     */
    public function transform($data, string $to, array $context = []): Crypto
    {
        $this->validator->validate($data);

        return new Crypto(
            $data->getName(),
            $data->getIssuer(),
            $data->getValue()
        );
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Crypto) {
            return false;
        }

        return Crypto::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
