<?php

namespace App\DataTransformer\Input\User;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\Input\User\UserInputDto;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserInputDataTransformer implements DataTransformerInterface
{
    private ValidatorInterface $validator;
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(ValidatorInterface $validator, UserPasswordHasherInterface $passwordHasher)
    {
        $this->validator = $validator;
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * @param UserInputDto $data
     */
    public function transform($data, string $to, array $context = []): User
    {
        $this->validator->validate($data);

        $user = new User(
            $data->getEmail(),
            $data->getPassword(),
            $data->getFirstName(),
            $data->getLastName()
        );

        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $user->getPassword()
        );

        $user->setPassword($hashedPassword);

        return $user;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof User) {
            return false;
        }

        return User::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
