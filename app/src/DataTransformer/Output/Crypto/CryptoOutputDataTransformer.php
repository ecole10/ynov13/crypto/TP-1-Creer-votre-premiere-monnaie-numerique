<?php

namespace App\DataTransformer\Output\Crypto;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\CryptoOutputDto;
use App\Entity\Crypto;

final class CryptoOutputDataTransformer implements DataTransformerInterface
{
    /**
     * @param Crypto $data
     */
    public function transform($data, string $to, array $context = []): CryptoOutputDto
    {
        return new CryptoOutputDto(
            $data->getId(),
            $data->getName(),
            $data->getIssuer(),
            $data->getValue()
        );
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return CryptoOutputDto::class === $to && $data instanceof Crypto;
    }
}
