<?php

namespace App\DataTransformer\Output\Transaction;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\TransactionOutputDto;
use App\Entity\Transaction;
use App\Entity\User;

final class TransactionOutputDataTransformer implements DataTransformerInterface
{
    /**
     * @param User $data
     */
    public function transform($data, string $to, array $context = []): TransactionOutputDto
    {
        /** @var Transaction $transaction */
        $transaction = $data->getWallet()->getTransactions()->last();

        return new TransactionOutputDto(
            $transaction->getId(),
            $transaction->getFromWallet()->getId(),
            $transaction->getToWallet()->getId(),
            $transaction->getCrypto()->getIssuer(),
            $transaction->getValue(),
            $transaction->getCreatedAt()
        );
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return TransactionOutputDto::class === $to && $data instanceof User;
    }
}
