<?php

namespace App\DataTransformer\Output\User;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\Output\UserOutputDto;
use App\Dto\Output\WalletOutputDto;
use App\Entity\User;

final class UserOutputDataTransformer implements DataTransformerInterface
{
    /**
     * @param User $data
     */
    public function transform($data, string $to, array $context = []): UserOutputDto
    {
        return new UserOutputDto(
            $data->getId(),
            $data->getEmail(),
            $data->getFirstName(),
            $data->getLastName(),
            new WalletOutputDto($data->getWallet()->getId())
        );
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return UserOutputDto::class === $to && $data instanceof User;
    }
}
