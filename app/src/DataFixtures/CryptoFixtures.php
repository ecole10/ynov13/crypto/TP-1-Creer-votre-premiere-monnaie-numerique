<?php

namespace App\DataFixtures;

use App\Entity\Crypto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CryptoFixtures extends Fixture
{
    public const BITCOIN = 'btc';
    public const POKEN = 'pkn';
    public const CARDANO_ADA = 'ada';

    public function load(ObjectManager $manager): void
    {
        $bitcoin = new Crypto('Bitcoin', 'BTC', 50000);
        $poken = new Crypto('Poken', 'PKN', 0.05);
        $cardano = new Crypto('Cardano', 'ADA', 1.5);

        $manager->persist($bitcoin);
        $manager->persist($poken);
        $manager->persist($cardano);

        $manager->flush();

        $this->addReference(self::BITCOIN, $bitcoin);
        $this->addReference(self::POKEN, $poken);
        $this->addReference(self::CARDANO_ADA, $cardano);
    }
}
