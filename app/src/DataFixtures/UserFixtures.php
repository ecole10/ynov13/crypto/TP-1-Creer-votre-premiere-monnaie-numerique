<?php

namespace App\DataFixtures;

use App\Entity\Crypto;
use App\Entity\User;
use App\Entity\WalletCrypto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $john = new User('john@doe.com', 'password', 'john', 'doe');

        $hashedPassword = $this->passwordHasher->hashPassword(
            $john,
            $john->getPassword()
        );

        $john->setPassword($hashedPassword);

        $jane = new User('jane@doe.com', 'password', 'jane', 'doe');

        $hashedPassword = $this->passwordHasher->hashPassword(
            $jane,
            $jane->getPassword()
        );

        $jane->setPassword($hashedPassword);

        $manager->persist($john);
        $manager->persist($jane);
        $manager->flush();

        $john->getWallet()->addWalletCrypto(
            $this->getWalletCrypto(
                $this->getReference(CryptoFixtures::BITCOIN),
                2
            )
        );

        $john->getWallet()->addWalletCrypto(
            $this->getWalletCrypto(
                $this->getReference(CryptoFixtures::POKEN),
                5000
            )
        );

        $john->getWallet()->addWalletCrypto(
            $this->getWalletCrypto(
                $this->getReference(CryptoFixtures::CARDANO_ADA),
                100
            )
        );

        $manager->persist($john);
        $manager->flush();
    }

    /**
     * @param Crypto $crypto
     */
    private function getWalletCrypto(object $crypto, float $number): WalletCrypto
    {
        $walletCrypto = new WalletCrypto();
        $walletCrypto
            ->setCrypto($crypto)
            ->setNumber($number);

        return $walletCrypto;
    }

    public function getDependencies(): array
    {
        return [
            CryptoFixtures::class,
        ];
    }
}
