<?php

namespace App\Entity;

use App\Repository\TransactionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TransactionRepository::class)
 */
class Transaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Wallet::class, inversedBy="transactions", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fromWallet;

    /**
     * @ORM\ManyToOne(targetEntity=Wallet::class, inversedBy="transactions", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $toWallet;

    /**
     * @ORM\ManyToOne(targetEntity=Crypto::class, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $crypto;

    /**
     * @ORM\Column(type="float")
     */
    private $value;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFromWallet(): ?Wallet
    {
        return $this->fromWallet;
    }

    public function setFromWallet(?Wallet $fromWallet): self
    {
        $this->fromWallet = $fromWallet;

        return $this;
    }

    public function getToWallet(): ?Wallet
    {
        return $this->toWallet;
    }

    public function setToWallet(?Wallet $toWallet): self
    {
        $this->toWallet = $toWallet;

        return $this;
    }

    public function getCrypto(): ?Crypto
    {
        return $this->crypto;
    }

    public function setCrypto(?Crypto $crypto): self
    {
        $this->crypto = $crypto;

        return $this;
    }

    public function getValue(): ?float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
