<?php

namespace App\Entity;

use App\Repository\WalletRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WalletRepository::class)
 */
class Wallet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="wallet")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $user = null;

    /**
     * @ORM\OneToMany(targetEntity=WalletCrypto::class, mappedBy="wallet", cascade={"persist"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    private Collection $walletCryptos;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="fromWallet", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    private Collection $transactions;

    public function __construct()
    {
        $this->walletCryptos = new ArrayCollection();
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getWalletCryptos(): Collection
    {
        return $this->walletCryptos;
    }

    public function getWalletCryptoByIssuer(string $issuer): ?WalletCrypto
    {
        if ($this->isContainsCrypto($issuer)) {
            return $this->getWalletCryptos()->filter(function(WalletCrypto $walletCrypto) use ($issuer) {
                return $walletCrypto->getCrypto()->getIssuer() === $issuer;
            })->current();
        }

        return null;
    }

    private function isContainsCrypto(string $cryptoIssuer): bool
    {
        return $this->getWalletCryptos()->exists(function(int $key, WalletCrypto $walletCrypto) use ($cryptoIssuer) {
            return $walletCrypto->getCrypto()->getIssuer() === $cryptoIssuer;
        });
    }

    public function addWalletCrypto(WalletCrypto $walletCrypto): self
    {
        if (!$this->walletCryptos->contains($walletCrypto)) {
            $this->walletCryptos[] = $walletCrypto;
            $walletCrypto->setWallet($this);
        }

        return $this;
    }

    public function removeWalletCrypto(WalletCrypto $walletCrypto): self
    {
        if ($this->walletCryptos->removeElement($walletCrypto)) {
            if ($walletCrypto->getWallet() === $this) {
                $walletCrypto->setWallet(null);
            }
        }

        return $this;
    }

    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setFromWallet($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): self
    {
        if ($this->transactions->removeElement($transaction)) {
            if ($transaction->getFromWallet() === $this) {
                $transaction->setFromWallet(null);
            }
        }

        return $this;
    }
}
