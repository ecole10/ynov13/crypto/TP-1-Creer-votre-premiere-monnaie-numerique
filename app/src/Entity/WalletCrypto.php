<?php

namespace App\Entity;

use App\Repository\WalletCryptoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WalletCryptoRepository::class)
 */
class WalletCrypto
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Wallet::class, inversedBy="walletCryptos", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $wallet;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity=Crypto::class, fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $crypto;

    /**
     * @ORM\Column(type="float")
     */
    private float $number;

    public function getWallet(): ?Wallet
    {
        return $this->wallet;
    }

    public function setWallet(?Wallet $wallet): self
    {
        $this->wallet = $wallet;

        return $this;
    }

    public function getCrypto(): ?Crypto
    {
        return $this->crypto;
    }

    public function setCrypto(?Crypto $crypto): self
    {
        $this->crypto = $crypto;

        return $this;
    }

    public function getNumber(): ?float
    {
        return $this->number;
    }

    public function setNumber(float $number): self
    {
        $this->number = $number;

        return $this;
    }
}
