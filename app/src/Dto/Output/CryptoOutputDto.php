<?php

namespace App\Dto\Output;

final class CryptoOutputDto
{
    private int $id;
    private string $name;
    private string $issuer;
    private float $value;

    public function __construct(int $id, string $name, string $issuer, float $value)
    {
        $this->id = $id;
        $this->name = $name;
        $this->issuer = $issuer;
        $this->value = $value;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getIssuer(): string
    {
        return $this->issuer;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}
