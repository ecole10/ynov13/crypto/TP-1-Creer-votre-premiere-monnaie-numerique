<?php

namespace App\Dto\Output;

final class WalletOutputDto
{
    private int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
