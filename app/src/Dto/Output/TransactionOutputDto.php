<?php

namespace App\Dto\Output;

final class TransactionOutputDto
{
    private string $id;
    private string $fromWallet;
    private string $toWallet;
    private string $cryptoIssuer;
    private float $value;
    private \DateTimeImmutable $createdAt;

    public function __construct(
        string $id,
        string $fromWallet,
        string $toWallet,
        string $cryptoIssuer,
        float $value,
        \DateTimeImmutable $createdAt
    ) {
        $this->id = $id;
        $this->fromWallet = $fromWallet;
        $this->toWallet = $toWallet;
        $this->cryptoIssuer = $cryptoIssuer;
        $this->value = $value;
        $this->createdAt = $createdAt;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFromWallet(): string
    {
        return $this->fromWallet;
    }

    public function getToWallet(): string
    {
        return $this->toWallet;
    }

    public function getCryptoIssuer(): string
    {
        return $this->cryptoIssuer;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
