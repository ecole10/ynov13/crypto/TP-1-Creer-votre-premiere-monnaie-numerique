<?php

namespace App\Dto\Output;

final class UserOutputDto
{
    private int $id;
    private string $email;
    private string $firstName;
    private string $lastName;
    private WalletOutputDto $wallet;

    public function __construct(
        int $id,
        string $email,
        string $firstName,
        string $lastName,
        WalletOutputDto $wallet
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->wallet = $wallet;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getWallet(): WalletOutputDto
    {
        return $this->wallet;
    }
}
