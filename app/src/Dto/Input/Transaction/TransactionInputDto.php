<?php

namespace App\Dto\Input\Transaction;

use Symfony\Component\Validator\Constraints as Assert;

final class TransactionInputDto
{
    /**
     * @Assert\NotBlank
     */
    private string $to;
    /**
     * @Assert\NotBlank
     */
    private string $cryptoIssuer;
    /**
     * @Assert\GreaterThan(value="0")
     */
    private float $value;

    public function __construct(string $to, string $cryptoIssuer, float $value)
    {
        $this->to = $to;
        $this->cryptoIssuer = $cryptoIssuer;
        $this->value = $value;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function getCryptoIssuer(): string
    {
        return $this->cryptoIssuer;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}
