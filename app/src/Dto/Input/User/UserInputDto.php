<?php

namespace App\Dto\Input\User;

use Symfony\Component\Validator\Constraints as Assert;

final class UserInputDto
{
    /**
     * @Assert\NotBlank
     * @Assert\Email
     */
    private string $email;
    /**
     * @Assert\NotBlank
     * @Assert\Length(min=8, max=150)
     */
    private string $password;
    /**
     * @Assert\NotBlank
     */
    private string $firstName;
    /**
     * @Assert\NotBlank
     */
    private string $lastName;

    public function __construct(string $email, string $password, string $firstName, string $lastName)
    {
        $this->email = $email;
        $this->password = $password;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}
