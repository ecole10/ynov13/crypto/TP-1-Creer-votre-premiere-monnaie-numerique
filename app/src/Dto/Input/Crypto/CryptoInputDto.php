<?php

namespace App\Dto\Input\Crypto;

use Symfony\Component\Validator\Constraints as Assert;

final class CryptoInputDto
{
    /**
     * @Assert\NotBlank
     */
    private string $name;
    /**
     * @Assert\NotBlank
     */
    private string $issuer;
    /**
     * @Assert\GreaterThan(value="0")
     */
    private float $value;

    public function __construct(string $name, string $issuer, float $value)
    {
        $this->name = $name;
        $this->issuer = $issuer;
        $this->value = $value;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getIssuer(): string
    {
        return $this->issuer;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}
