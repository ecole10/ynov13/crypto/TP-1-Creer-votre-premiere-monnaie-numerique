# TP - Créer votre première monnaie numérique !

## Cahier des charges

- Base de données
- S’inscrire (avoir une adresse)
- Envoyer / Recevoir
- Pas de réseau (P2P)

## Objectif

Envoyer et recevoir une monnaie

## Conseils

1. Choisir son langage (ex : Python)
2. Choisir sa base de données (ex : SQLite)
3. Faire un modèle conceptuel de données
4. Faire un CRUD (https://nouvelle-techno.fr/articles/live-coding-creer-un-crud-en-php)
